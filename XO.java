
import java.util.Scanner;

public class XO {
	
	private static String showWelcome() {
		String welcome = "-------- Welcome to OX GAME --------";
		return welcome;
	}

	private static void showTable(char[][] TableOX) {
		for (int i = 0; i < 3; i++) {
			System.out.println("+---+---+---+");
			for (int j = 0; j < 3; j++) {
				System.out.print("| ");
				System.out.print(TableOX[i][j]);
				System.out.print(" ");
			}
			System.out.print("| ");
			System.out.println();
		}
		System.out.println("+---+---+---+");
	}

	private static int turnO(int endgame,int turn,int count ,char[][] tableOX) {
		Scanner kb = new Scanner(System.in);
		System.out.println("turn O");
		System.out.print("Please input row and collum : ");
		int row = kb.nextInt()-1;
		int col = kb.nextInt()-1;
		insertO(endgame,turn,count ,tableOX,row,col);
		return turn=1;
	}

	private static void insertO(int endgame,int turn,int count ,char[][] tableOX,int row,int col){
		if (row > 2 || row < 0 || col > 2 || col < 0 || tableOX[row][col] == 'O' || tableOX[row][col] == 'X') {
			checkError(row, col, tableOX,endgame);
			turnO(endgame,turn,count,tableOX);
		} else {
			tableOX[row][col] = 'O';
		}
	}

	private static int turnX(int endgame,int turn,int count,char[][] tableOX) {
		Scanner kb = new Scanner(System.in);
		System.out.println("turn X");
		System.out.print("Please input row and collum : ");
		int row = kb.nextInt()-1;
		int col = kb.nextInt()-1;
		insertX(endgame,turn,count , tableOX,row,col);
		return turn=0;
	}

	private static void insertX(int endgame,int turn,int count ,char[][] tableOX,int row,int col){
		if (row > 2 || row < 0 || col > 2 || col < 0 || tableOX[row][col] == 'O' || tableOX[row][col] == 'X') {
			checkError(row, col, tableOX,endgame);
			turnX(endgame,turn,count,tableOX);
		} else {
			tableOX[row][col] = 'X';
		}
	}
	
	private static int checkError(int row, int col, char[][] tableOX,int endgame) {
		if ((col > 2 || col < 0) && (row > 2 || row < 0)) {
			System.out.println("Error!! Row and Collum : Please input 1 or 2 or 3 \n");
		} else if (row > 2 || row < 0) {
			System.out.println("Error!! Row : Please input 1 or 2 or 3 \n");
		} else if (col > 2 || col < 0) {
			System.out.println("Error!! Collum : Please input 1 or 2 or 3 \n");
		} else if (tableOX[row][col] == 'O' || tableOX[row][col] == 'X') {
			System.out.println("it has already been chosen. \n");
		}
		return endgame;
	}

	private static int checkTie(int count, char[][] tableOX,int endgame) {
		if (count == 9&&endgame==0) {
			showTable(tableOX);
			System.out.println("It's a tie.");
			endgame++;
		}
		return endgame;
	}

	private static int checkOWinRow(int r, int c, char[][] tableOX,int endgame) {
		if ((tableOX[r][c] == 'O' && tableOX[r][c + 1] == 'O' && tableOX[r][c + 2] == 'O')
				|| (tableOX[r + 1][c] == 'O' && tableOX[r + 1][c + 1] == 'O' && tableOX[r + 1][c + 2] == 'O')
				|| (tableOX[r + 2][c] == 'O' && tableOX[r + 2][c + 1] == 'O' && tableOX[r + 2][c + 2] == 'O')) {
			showTable(tableOX);
			System.out.println("Player O is winner!!");
			endgame++;
		}
		return endgame;
	}

	private static int checkOWinCol(int r, int c, char[][] tableOX,int endgame) {
		if ((tableOX[r][c] == 'O' && tableOX[r + 1][c] == 'O' && tableOX[r + 2][c] == 'O')
				|| (tableOX[r][c + 1] == 'O' && tableOX[r + 1][c + 1] == 'O' && tableOX[r + 2][c + 1] == 'O')
				|| (tableOX[r][c + 2] == 'O' && tableOX[r + 1][c + 2] == 'O' && tableOX[r + 2][c + 2] == 'O')) {
			showTable(tableOX);
			System.out.println("Player O is winner!!");
			endgame++;
		}
		return endgame;
	}

	private static int checkOWinX(int r, int c, char[][] tableOX,int endgame) {
		if ((tableOX[0][0] == 'O' && tableOX[1][1] == 'O' && tableOX[2][2] == 'O')
				|| (tableOX[0][2] == 'O' && tableOX[1][1] == 'O' && tableOX[2][0] == 'O')) {
			showTable(tableOX);
			System.out.println("Player O is winner!!");
			endgame++;
		}
		return endgame;
	}

	private static int checkXWinRow(int r, int c, char[][] tableOX,int endgame) {
		if ((tableOX[r][c] == 'X' && tableOX[r][c + 1] == 'X' && tableOX[r][c + 2] == 'X')
				|| (tableOX[r + 1][c] == 'X' && tableOX[r + 1][c + 1] == 'X' && tableOX[r + 1][c + 2] == 'X')
				|| (tableOX[r + 2][c] == 'X' && tableOX[r + 2][c + 1] == 'X' && tableOX[r + 2][c + 2] == 'X')) {
			showTable(tableOX);
			System.out.println("Player X is winner!!");
			endgame++;
		}
		return endgame;
	}

	private static int checkXWinCol(int r, int c, char[][] tableOX,int endgame) {
		if ((tableOX[r][c] == 'X' && tableOX[r + 1][c] == 'X' && tableOX[r + 2][c] == 'X')
				|| (tableOX[r][c + 1] == 'X' && tableOX[r + 1][c + 1] == 'X' && tableOX[r + 2][c + 1] == 'X')
				|| (tableOX[r][c + 2] == 'X' && tableOX[r + 1][c + 2] == 'X' && tableOX[r + 2][c + 2] == 'X')) {
			showTable(tableOX);
			System.out.println("Player X is winner!!");
			endgame++;
		}
		return endgame;
	}

	private static int checkXWinX(int r, int c, char[][] tableOX,int endgame) {
		if ((tableOX[0][0] == 'X' && tableOX[1][1] == 'X' && tableOX[2][2] == 'X')
				|| (tableOX[0][2] == 'X' && tableOX[1][1] == 'X' && tableOX[2][0] == 'X')) {
			showTable(tableOX);
			System.out.println("Player X is winner!!");
			endgame++;
		}
		return endgame;
	}
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		char[][] tableOX = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
		int endgame=0;
		int row = 0;
		int col = 0;
		int count = 0;
		int turn = 0;
		System.out.println(showWelcome());
		char ans = 'Y';
		while (true) {
			if (ans == 'N') {
				System.out.println("Let's play again next time!!");
				break;
			}else if (ans == 'Y') {
				while (endgame == 0) {
					showTable(tableOX);
					turnO(endgame,count,turn,tableOX);
					count++;
					if(	checkOWinCol(row, col, tableOX,endgame)>0||
						checkOWinRow(row, col, tableOX,endgame)>0||
						checkOWinX(row, col, tableOX,endgame)>0 || 
						checkTie(count, tableOX,endgame)>0&&endgame==0){
						endgame++;
					}else if (endgame == 0) {
						showTable(tableOX);
						turnX(endgame,count,turn,tableOX);
						count++;
						if(checkXWinCol(row, col, tableOX,endgame)>0||
							checkXWinRow(row, col, tableOX,endgame)>0||
							checkXWinX(row, col, tableOX,endgame)>0 || 
							checkTie(count,tableOX,endgame)>0&&endgame==0){
							endgame++;
						}
					}
					if (endgame != 0) {
						System.out.println("Game is over. Do you want to play again?");
						System.out.print("Please input Y/N : ");
						ans = kb.next().charAt(0);
						if (ans == 'Y') {
							endgame = 0;
							count = 0;
							System.out.println("-------- One More!! --------");
							for (int i = 0; i < 3; i++) {
								for (int j = 0; j < 3; j++) {
									tableOX[i][j] = '-';
								}
							}
						}

					}
				}

			}else{
				if (endgame != 0) {
					System.out.print("Please input Only Y or N : ");
					ans = kb.next().charAt(0);
				}
			}
		}

	}
}
